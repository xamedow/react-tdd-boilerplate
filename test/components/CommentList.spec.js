import React from 'react';
import sinon from 'sinon';

// Once we set up Karma to run our tests through webpack
// we will no longer need to have these long relative paths

import CommentList from 'components/CommentList';
import {
    mount,
    shallow,
    spyLifecycle
} from 'enzyme';

describe('(Component) CommentList', () => {
        
    it('calls componentDidMount', () => {
        sinon.spy(CommentList.prototype, 'componentDidMount');

        const props = {
            onMount: () => {
            }, // An anonymous function in ES6 syntax.
            isActive: false
        };

        // using destructuring to pass props down
        // easily and then mounting the component
        mount(<CommentList {...props} />);

        // CommentLists's componentDidMount should have been
        // called once. spyLifecycle attaches sinon spys so we can
        // make this assertion
        expect(CommentList.prototype.componentDidMount.calledOnce).to.be.true;
    });

    it('calls onMount prop once it mounts', () => {
        // create a spy for the onMount function
        const props = {onMount: sinon.spy()};

        // mount our component
        mount(<CommentList {...props}/>);

        // expect that onMount was called
        expect(props.onMount.calledOnce).to.be.true;
    });

    it('should render as <ul>', () => {
        const props = {
            onMount: () => {
            }
        };
        const wrapper = shallow(<CommentList {...props}/>);
        expect(wrapper.type()).to.eql('ul');
    });

    describe('when active...', () => {
        const wrapper = shallow(<CommentList onMount={() => {}} isActive />);

        it('should render with className active-list', () => {
            expect(wrapper.prop('className')).to.eql('active-list');
        });
    });

    describe('when inactive...', () => {
        const wrapper = shallow(
            <CommentList onMount={() => {}} isActive={false} />
        );

        it('should render with className inactive-list', () => {
            expect(wrapper.prop('className')).to.eql('inactive-list');
        });
    })
});